# The Discretized Laplace Operator on Hyperrectangles with Zero Dirichlet Boundary Conditions

This repository contains Python 2 code generating stiffness and mass matrix as well as the eigenpairs of the Laplace operator on hyperrectangles discretized with the finite difference method (FDM) and the finite element method (FEM) on equidistant grids.

For a d-dimensional domain `(0,l1) x (0,l2) x ... x (0,ld)` with `n1`, `n2`, ..., `nd` interior grid points, the corresponding FDM matrix can be constructed with 
```
A = fdm_laplacian([n1,n2,...,nd], [l1,l2,...,ld])
```
(ellipsis must be replaced of course) and the eigenpairs with
```
d, X = fdm_laplacian_eigenpairs([n1,n2,...,nd], [l1,l2,...,ld])
```
For the FEM, the equivalent code is
```
K, M = fem_laplacian([n1,n2,...,nd], [l1,l2,...,ld])
d, X = fem_laplacian_eigenpairs([n1,n2,...,nd], [l1,l2,...,ld])
```

The code uses NumPy and SciPy.
